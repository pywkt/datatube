"""
Python script to convert the contents of a YouTube video
to a blog post written with markdown syntax.
"""

import argparse
import concurrent.futures
import glob
import os
import string
from os.path import exists
import ffmpeg
import requests
import whisper
import yt_dlp
from slugify import slugify

parser = argparse.ArgumentParser()
parser.add_argument("-u", "--url", type=str)
parser.add_argument("-s", "--satan", type=str)

args = parser.parse_args()
yt_url = args.url
satan_mode = args.satan

GEN_URL = "http://localhost:11434/api/generate/"
CHAT_URL = "http://localhost:11434/api/chat/"
MODEL = "mistral"
STREAM = False

def send_request_to_llama(url, data):
    """
    Sends a POST request to the Llama API with the specified URL and data.
    
    Args:
        url (str): The URL of the Llama API endpoint.
        data (dict): The data to be sent in the POST request.
        
    Returns:
        The response from the Llama API, either a JSON object or a string 
        depending on the value of `url`.
    
    Raises:
        requests.exceptions.RequestException: If there was an error making the request.
    """
    post_req = requests.post(url, json=data)
    post_json = post_req.json()
    return post_json["response"] if url == GEN_URL else post_json["message"]["content"]


# ── File info ───────────────────────────────────────────────────────────

def check_dirs():
    """
    Check if the directories for audio, text, video, and markdown files exist.
    If they do not exist, create them.
    
    Returns: None
    """
    audio_dir = "audio"
    text_dir = "text"
    video_dir = "video"
    md_dir = "markdown"
    audio_dir_exists = os.path.isdir(audio_dir)
    text_dir_exists = os.path.isdir(text_dir)
    video_dir_exists = os.path.isdir(video_dir)
    md_dir_exists = os.path.isdir(md_dir)

    if not audio_dir_exists:
        os.makedirs(audio_dir)
        print("Created ./audio/")

    if not text_dir_exists:
        os.makedirs(text_dir)
        print("Created ./text/")

    if not video_dir_exists:
        os.makedirs(video_dir)
        print("Created ./video/")

    if not md_dir_exists:
        os.makedirs(md_dir)
        print("Created ./markdown/")


# ── Download video (youtube-dl) ─────────────────────────────────────────

def download_video():
    """
    Downloads a video from YouTube and converts it to audio using yt-dlp.
    
    Returns:
        A dictionary containing the file name and type of the downloaded audio file, 
        or an error code if there was an issue with the download.
        
    Raises:
        requests.exceptions.RequestException: If there was an error making the request.
    """

    ydl_opts = {
        "paths": {"home": "./audio/"},
        "output": "%(title)s.%(ext)s",
        "format": "m4a/bestaudio/best",
        "quiet": True,
        "postprocessors": [{"key": "FFmpegExtractAudio", "preferredcodec": "wav"}],
    }

    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        error_code = ydl.download(yt_url)
        if error_code > 0:
            print("Error downloading video/audio:", error_code)

            return error_code
        # else:
        files = glob.glob("./audio/*")
        latest_file = max(files, key=os.path.getctime)
        filename_whole = latest_file.split("/")[-1]
        filename = filename_whole.rsplit(".", 1)[0]
        filetype = filename_whole.split(".")[-1]
        slug_name = slugify(filename)
        old_name = os.path.join("./audio/", filename_whole)
        new_name = os.path.join("./audio/", slug_name + "." + filetype)
        os.rename(old_name, new_name)

        return {"f_name": slug_name.split(".")[0], "f_type": filetype}


# ── Convert video to audio (ffmpeg) ─────────────────────────────────────

def video_to_audio(filename, filetype):
    """Extracts the audio from a video file and saves it"""

    print("*** Extracting audio")
    (
        ffmpeg.input(f"./video/{filename}.{filetype}")
        .output(f"./audio/{filename}.{filetype}")
        .run(quiet=True, overwrite_output=True)
    )


# ── Transcribe audio (whisper) ──────────────────────────────────────────

def audio_to_text(filename, filetype):
    """
    Converts an audio file to text using the Whisper library.

    Parameters:
        filename : str
            The name of the audio file to be converted to text.
        filetype : str
            The type of the audio file (e.g., "wav", "mp3").

    Returns:
        text_content : list[str]
            A list of strings containing the transcribed text from the audio file.

    Raises:
        FileNotFoundError
            If the specified audio file does not exist or cannot be read.
    """

    has_audio = exists(f"./audio/{filename}.{filetype}")
    has_text = exists(f"./text/{filename}.txt")

    print("=====")
    print(f"*** {filename[0:60]}...")
    print("=====")

    if has_text:
        print("*** Transcription exists, skipping.")
        with open(f"./text/{filename}.txt", encoding="utf8") as t:
            text_content = t.readlines()
        return text_content[0]

    if has_audio:
        print("*** Transcribing...")
        w_model = whisper.load_model("medium")
        w_result = w_model.transcribe(f"./audio/{filename}.{filetype}")

        w_prompt = w_result["text"]

        with open(f"./text/{filename}.txt", "w", encoding="utf8") as data:
            for trans_line in w_prompt:
                data.writelines("\n".join(trans_line))

        return w_prompt

    print("Audio track not found")
    return "Audio track not found"


# ── Write post (ollama) ─────────────────────────────────────────────────

def write_post(text):
    """
    Parameters:
        text : str
            The text transcription to be used as the source for writing the blog post
            
    Returns:
    """

    def get_subject(content):
        role_post_data = {
            "model": MODEL,
            "stream": STREAM,
            "messages": [
                {"role": "system", "content": content},
                {
                    "role": "user",
                    "content": (
                        """
                        Describe the subject of the text above. Your response should be one word 
                        and it can only be one of the following choices: 'food', 'travel', 'tech',
                        'plants' or 'HVAC'. If none of those options describe the content respond 
                        only with the word 'general'. Do not explain anything and only respond 
                        with ONE WORD.
                        """
                    ),
                },
            ],
        }

        return send_request_to_llama(CHAT_URL, role_post_data)

    persona = get_subject(text)

    def get_persona(persona):
        if satan_mode:
            persona = "satan"
        else:
            persona = persona.lower().translate(
                str.maketrans("", "", string.punctuation)
            )

        print("*** Subject:", persona)

        if "plants" in persona:
            return "You are a highly educated botanist and avid gardener. \
You have a vast knowledge of horticulture and bio-science. \
You run a gardening blog where you post about how to grow plants \
and all other things related to plants."

        if "food" in persona:
            return "You are an professional chef named Theo who has \
cooked in several five-star restaurants and has studied food \
science. You run a food blog called 'Dude Food' where you \
talk about all things related to food and cooking."

        if "hvac" in persona:
            return "You are an HVAC technician named Steve. You can solve any\
problem related to heating, cooling or plumbing. You run a blog \
where you help people learn and understand all about HVAC topics."

        if "travel" in persona:
            return "You are a world traveller who has been to almost every \
country in the world. You have vast knowledge of all cultures and traditions."

        if "satan" in persona:
            return "You are the son of satan. You are very evil and you \
hate everything and everyone. Nothing makes you happier than listening to \
heavy metal and watching humans suffer."

        return "You are a friendly, clever, funny, educated 30-something \
year old blogger named Julia. You run a blog where you talk about all the \
things you love."

    role = get_persona(persona)

    post_data = {
        "model": MODEL,
        "stream": STREAM,
        "prompt": f"{role} Rewrite the following text in the form of a blog \
post. It should read very natural and unscripted, and written in 'casual' \
English. Be informative, but do not over-explain details or use \
unnecessary words. Include a title with the post. Do not include any \
metadata and do not explain who is talking. Do not use your name in \
the post. Do not quote the following text directly. Do not ever \
mention that this is a video or that people should subscribe. \
Do not mention 'the comments'. " + text,
    }

    print("*** Writing post...")

    return send_request_to_llama(GEN_URL, post_data)


# ── Convert to markdown ─────────────────────────────────────────────────

def convert_to_markdown(post_text):
    """
    Converts the given blog post to markdown syntax, including titles for paragraphs when needed.

    Parameters:
        post_text (str): The text of the blog post to be converted.

    Returns:
        str: The converted blog post in markdown format.
    """
    print("*** Generating markdown...")
    instructions = """Take the following blog post and convert it to markdown
                    syntax. Include titles for paragraphs when needed. Do not
                    include any music indicators, metadata or indications
                    that this is a transcript. Do not wrap the response in a
                    code block. Do not change the content of the blog post."""

    prompt = instructions + post_text
    post_data = {"model": MODEL, "prompt": prompt, "stream": STREAM}
    markdown_res = send_request_to_llama(GEN_URL, post_data)

    return markdown_res


# ──────────────────────────────────────────────────────────────────────

check_dirs()

with concurrent.futures.ThreadPoolExecutor() as executor:
    get_audio = executor.submit(download_video)
    audio = get_audio.result()

    transcribe = executor.submit(audio_to_text, audio["f_name"], audio["f_type"])

    transcribe_text = transcribe.result()

    if transcribe.done():
        make_post = executor.submit(write_post, transcribe_text)
        post_result = make_post.result()

    markdown = convert_to_markdown(post_result)


with open(f"./markdown/{audio['f_name'][0:-12]}.md", "w", encoding="utf8") as f:
    for line in markdown:
        f.writelines("\n".join(line))
    print(">> Done")
    print(f"Saved to ./markdown/{audio['f_name'][0:-12]}.md")
